# Autor: Steven Guaman     # steven.guaman@unl.edu.ec
'''solicitar puntuacion cuantitava y presentar respuesta cualitativa'''

print ('BIENVENIDO')

try:
    puntuacion = float(input ('Ingresa una calificación entre 0.0 y 1.0: '))

    if puntuacion >=0 and puntuacion <= 1.0:
        if puntuacion >= 0.9:
            print ('Sobresaliente')
        elif puntuacion >= 0.8:
            print ('Notable')
        elif puntuacion >= 0.7:
            print ('Bien')
        elif puntuacion >= 0.6:
            print ('Suficiente')
        elif puntuacion < 0.6:
            print ('Insuficiente')
    else:
        print ('Ingrese un número positivo')
except:
    print ('Puntuación incorrecta')