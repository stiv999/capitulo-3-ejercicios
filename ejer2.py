# Autor: Steven Guaman     # steven.guaman@unl.edu.ec
'''Usar try and except en el ejercicio del salario'''

print ('BIENVENIDO\n vamos a calcular un salario')

try:
    h_t = float(input('Ingrese el número de horas trabajadas:'))
    t_h = float(input('Ingrese la tarifa por hora:'))

    reslt = h_t * t_h
    if h_t > 0  and h_t <= 40:
        print(f'El salario total es de: {reslt:.2f} $')
    elif h_t >40:
        h_e = (h_t-40)
        print(f'El salario normal es de: {reslt:.2f} $')
        print ('horas extra: ', h_e)
        print ('salario de horas extra: ', (h_e*1.5))
        reslt = reslt + (h_e*1.5)
        print(f'El salario total es de: {reslt:.2f} $')
    else:
        print ('Por favor introduzca un número positivo')
except:
    print('Por favor introduzca un número')